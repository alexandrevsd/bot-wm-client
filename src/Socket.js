import { io } from "socket.io-client";
const ENDPOINT = "http://localhost:8081";
const socket = io(ENDPOINT);

class Socket {

    static getSocket() {
        return socket;
    }

    static getPlayerInfos(serverId) {
        socket.emit('getPlayer', serverId);
        return socket.on(serverId + 'player', (player) => {
            return player;
        })
    }

    static listenServer(serverId) {
        function getPlayer() {
            socket.emit('getPlayer', serverId);
        }
        return setInterval(getPlayer, 1000);
    }


}

export default Socket;