import $ from "jquery";
import React from "react";

class Utils {

    static randomKey() {
        const letters = ['x', 'y', 'z', 'w', 'v', 't'];
        const letterIndex = Math.floor(Math.random()*5);
        return letters[letterIndex] + Math.floor(Math.random()*10000000);
    }

    static modal(title, body, buttons, app) {
        app.setState({modal: {title, body, buttons}})
        $('#modal').modal();
    }

    static infoModal(title, message, app) {
        const buttons = <>
            <button className="btn btn-secondary" data-dismiss="modal">Fermer</button>
        </>;
        const body = <>{message}</>
        app.setState({modal: {title, body, buttons}})
        $('#modal').modal();
    }

    static getSongDuration(length) {
        const date = new Date(length);
        let hours = date.getUTCHours() < 10 ? '0' + date.getUTCHours() : date.getUTCHours();
        const minutes = date.getUTCMinutes() < 10 ? '0' + date.getUTCMinutes() : date.getUTCMinutes();
        const seconds = date.getUTCSeconds() < 10 ? '0' + date.getUTCSeconds() : date.getUTCSeconds()
        hours = hours === '00' || hours === 0 ? '' : hours + ':';
        return hours + minutes + ':' + seconds;
    }

    static getSongLength(service, length) {
        if (service === 'youtube' || service === 'vk') {
            length *= 1000;
        }

        if (service === 'formatted') {
            const values = length.split(':').reverse();
            length = 0;
            values.forEach((value, index) => {
                length += (index > 0 ? parseInt(value) * (60 * (index)) : parseInt(value)) * 1000;
            })
        }

        return length;
    }

    static convertHTMLEntity(text) {
        const span = document.createElement('span');

        return text
            .replace(/&[#A-Za-z0-9]+;/gi, (entity, position, text) => {
                span.innerHTML = entity;
                return span.innerText;
            });
    }

    static capitalize(string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
    }
}

export default Utils;