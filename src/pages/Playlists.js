import React from "react";
import {Link} from "react-router-dom";
import Api from "../Api";
import Utils from "../Utils";

class Playlists extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            publicPlaylists: [],
            ownedPlaylists: []
        }
    }

    async componentDidMount() {
        const userId = this.props.app.user.id;
        const serverId = this.props.app.server;
        const result = await Api.postBot('/api/music/playlists', userId, serverId, ['owned']);
        if(result['code'] === 200) {
            this.setState({ownedPlaylists: result['playlists']});
            console.log(result['playlists'])
        }
        const result2 = await Api.postBot('/api/music/playlists', userId, serverId);
        if (result2['code'] === 200) {
            this.setState({publicPlaylists: result2['playlists']});
        }
    }

    handlePlay = async (playlistId) => {
        const userId = this.props.app.user.id;
        const serverId = this.props.app.server;
        console.log(await Api.postBot('/api/music/playlist/play', userId, serverId, [playlistId]));
    }

    handleAddToQueue = async (playlistId) => {
        const userId = this.props.app.user.id;
        const serverId = this.props.app.server;
        console.log(await Api.postBot('/api/music/queue/addPlaylist', userId, serverId, [playlistId]));
    }

    handleAddToPlaylist = async (fromPlaylistId, toPlaylistId) => {
        const userId = this.props.app.user.id;
        const serverId = this.props.app.server;
        console.log(await Api.postBot('/api/music/playlist/addPlaylist', userId, serverId, [fromPlaylistId, toPlaylistId]));
    }

    render() {
        return <>
            <h1 className="h3 text-gray-300 mb-4 ml-4">Playlists <Link to={'/playlists/create'}
                                                                       className="btn btn-success"
                                                                       style={{float: 'right', marginRight: '10px'}}><i
                className="fa fa-plus text-gray-200"/></Link></h1>
            <table className={"table table-dark table-hover"}>
                <thead>
                <tr>
                    <th colSpan="100%">Tes playlists</th>
                </tr>
                </thead>
                <tbody>
                {this.state.ownedPlaylists.map(playlist => <tr>
                    <td>{Utils.getSongDuration(playlist.length)}</td>
                    <td>{playlist.title}</td>
                    <td>
                        <button onClick={() => this.handlePlay(playlist.id)} className="wm-not-button fa fa-play text-gray-200"/>
                        {(this.props.app.ownedPlaylists.length > 0 || this.props.app.playing) &&
                        <div className="dropdown d-inline">
                            <button className="wm-not-button fa fa-plus text-success" id={"addTo"}
                                    role="button"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"/>
                            <div className="dropdown-menu dropdown-menu-right shadow animated--fade-in "
                                 aria-labelledby={"addTo"}>
                                {this.props.app.playing &&
                                <button className="dropdown-item" onClick={() => this.handleAddToQueue(playlist.id)}>
                                    <i className="fas fa-list fa-sm fa-fw mr-2 text-gray-400"/>
                                    File d'attente
                                </button>}
                                {this.props.app.ownedPlaylists.map(ownedPlaylist => {
                                    return <button onClick={() => this.handleAddToPlaylist(playlist.id, ownedPlaylist.id)} className="dropdown-item">
                                        <i className="fas fa-compact-disc fa-sm fa-fw mr-2 text-gray-400"/>
                                        {ownedPlaylist.title}
                                    </button>
                                })}
                            </div>
                        </div>
                        }
                        <Link className="wm-not-button fa fa-edit text-warning" to={"/playlists/edit/" + playlist.id}/>
                    </td>
                </tr>)}
                </tbody>
                <thead>
                <tr>
                    <th colSpan="100%">Playlists publiques</th>
                </tr>
                </thead>
                <tbody>
                {this.state.publicPlaylists.map(playlist => <tr>
                    <td>{playlist.duration}</td>
                    <td>{playlist.ownerUsername} - {playlist.title}</td>
                    <td>
                        <button onClick={() => this.handlePlay(playlist.id)} className="wm-not-button fa fa-play text-gray-200"/>
                        {(this.props.app.ownedPlaylists.length > 0 || this.props.app.playing) &&
                        <div className="dropdown d-inline">
                            <button className="wm-not-button fa fa-plus text-success" id={"addTo"}
                                    role="button"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"/>
                            <div className="dropdown-menu dropdown-menu-right shadow animated--fade-in "
                                 aria-labelledby={"addTo"}>
                                {this.props.app.playing &&
                                <button className="dropdown-item" onClick={() => this.handleAddToQueue(playlist.id)}>
                                    <i className="fas fa-list fa-sm fa-fw mr-2 text-gray-400"/>
                                    File d'attente
                                </button>}
                                {this.props.app.ownedPlaylists.map(ownedPlaylist => {
                                    return <button onClick={() => this.handleAddToPlaylist(playlist.id, ownedPlaylist.id)} className="dropdown-item">
                                        <i className="fas fa-compact-disc fa-sm fa-fw mr-2 text-gray-400"/>
                                        {ownedPlaylist.title}
                                    </button>
                                })}
                            </div>
                        </div>
                        }
                        <Link className="wm-not-button fa fa-eye text-gray-200" to={"/playlists/see/" + playlist.id}/>
                    </td>
                </tr>)}
                </tbody>
            </table>
        </>
    }

}

export default Playlists;