import React from "react";
import LoginContainer from "../components/LoginContainer";
import Socket from "../Socket";

class Server extends React.Component {

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        Socket.getSocket().on('channel_joined', ({server, userId}) => {
            if(userId === this.props.app.user.id) {
                this.props.app.setState({server})
            }
        });
    }

    handleServerChange = (e) => e.target.value !== "" && this.props.app.setState({server: e.target.value});

    render() {
        return (
            <LoginContainer>
                <div className="text-center">
                    <h1 className="h4 text-gray-900 mt-2">
                        Tu dois te connecter à un salon vocal où le bot peut te voir
                    </h1>
                </div>
            </LoginContainer>
        );
    }

}

export default Server;