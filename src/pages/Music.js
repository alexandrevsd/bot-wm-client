import React from "react";
import PageContainer from "../components/PageContainer";
import Socket from "../Socket";
import Api from "../Api";
import {SortableContainer, SortableElement, SortableHandle} from "react-sortable-hoc";
import arrayMove from 'array-move';

class Music extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            queue: undefined,
            items: ['Item 1', 'Item 2', 'Item 3', 'Item 4', 'Item 5', 'Item 6']
        }
    }

    async componentDidMount() {
        const serverId = this.props.app.server;
        Socket.getSocket().emit('getQueue', serverId);
        Socket.getSocket().on(serverId + 'queue', ({queue}) => {
            this.setState({queue});
        });
    }

    handleAddSongToQueue = async (url) => {
        const userId = this.props.app.user.id;
        const serverId = this.props.app.server;
        await Api.postBot('/api/music/queue_add_song', userId, serverId, [url]);
    }

    handlePlayQueuePosition = async (position) => {
        const userId = this.props.app.user.id;
        const serverId = this.props.app.server;
        await Api.postBot('/api/music/queue_play_position', userId, serverId, [position]);
    }

    handleDeleteQueuePosition = async (position) => {
        const userId = this.props.app.user.id;
        const serverId = this.props.app.server;
        await Api.postBot('/api/music/queue_delete_position', userId, serverId, [position]);
    }

    onSortEnd = async ({oldIndex, newIndex}) => {
        const queue = {...this.state.queue};
        console.log(oldIndex)
        console.log(newIndex)
        if(oldIndex > queue.position && newIndex <= queue.position) {
            queue.position++;
        } else if(oldIndex < queue.position && newIndex >= queue.position) {
            queue.position--;
        }
        queue.songs = arrayMove(queue.songs, oldIndex, newIndex);
        this.setState({queue});
        const userId = this.props.app.user.id;
        const serverId = this.props.app.server;
        await Api.postBot('/api/music/queue_save', userId, serverId, [queue]);
    };

    handleAddToPlaylist = async (playlistId, song) => {
        const userId = this.props.app.user.id;
        const serverId = this.props.app.server;
        await Api.postBot('/api/music/playlist_add_songs', userId, serverId, [playlistId, [song]]);
    }

    render() {
        const DragHandle = SortableHandle(() => <span><i className="fa fa-bars text-primary"/></span>);

        const SortableItem = SortableElement(({value}) => {
            const key = value.key;
            const index = value.index;
            const song = value.song;
            const songClass = this.state.queue.position === index ? "wm-row-selected" : "";
            return <tr className={songClass} id={key}>
                <td>{this.state.queue.position !== index && <DragHandle />}</td>
                <td>{song.duration}</td>
                <td>{song.artist} - {song.title}</td>
                <td className="wm-table-buttons wm-sort-disabled">
                    {playButton(index)}
                    {addButton(song, key)}
                    {deleteButton(index, key)}
                </td>
            </tr>
        });
        const SortableList = SortableContainer(({items}) => {
            return (
                <table className={"table table-dark table-hover"}>
                    <tbody>
                    {items.map((song, index) => {
                        const key = 'x' + Math.floor(Math.random() * 1000000000);
                        return <SortableItem index={index} key={key} value={{song, key, index}}/>
                    })}
                    </tbody>
                </table>
            );
        });

        const playButton = (index) => <button onClick={() => this.handlePlayQueuePosition(index)}
                                              className="wm-not-button fa fa-play text-gray-200"/>


        const addButton = (song, trId) => <div className="dropdown d-inline">
            <button className="wm-not-button fa fa-plus text-success" id={"addTo" + trId}
                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"/>
            <div className="dropdown-menu dropdown-menu-right shadow animated--fade-in "
                 aria-labelledby={"addTo" + trId}>
                {this.props.app.playing && <button className="dropdown-item"
                                                   onClick={() => this.handleAddSongToQueue(song.url)}>
                    <i className="fas fa-list fa-sm fa-fw mr-2 text-gray-400"/>
                    File d'attente
                </button>}
                {this.props.app.ownedPlaylists.map(playlist => {
                    return <button onClick={() => this.handleAddToPlaylist(playlist.id, song)}
                                   className="dropdown-item">
                        <i className="fas fa-compact-disc fa-sm fa-fw mr-2 text-gray-400"/>
                        {playlist.title}
                    </button>
                })}
            </div>
        </div>

        const deleteButton = (index) => <button onClick={() => this.handleDeleteQueuePosition(index)}
                                                className="wm-not-button fa fa-trash text-danger"/>

        const isQueueFilled = this.state.queue && this.state.queue.songs.length > 0;

        return isQueueFilled ?
            <>
                <h1 className="h3 text-gray-300 mb-4 ml-4">File d'attente</h1>
                <SortableList helperClass={"wm-test-sort"} lockAxis={"y"} items={this.state.queue.songs} onSortEnd={this.onSortEnd} useDragHandle/>
            </>
            :
            <PageContainer title={"File d'attente"} app={this.props.app}>
                <p>Pour commencer, recherche une musique dans la barre ci-dessus.<br/>Tu peux y entrer une URL
                    Youtube ou Soundcloud (musique ou playlist), mais aussi des mots-clés.</p>
            </PageContainer>;
    }

}

export default Music;