import React from "react";

class Login extends React.Component {
    render() {
        return(
            <div className="container">
                <div className="row justify-content-center">
                    <div className="col-xl-10 col-lg-12 col-md-9">
                        <div className="card o-hidden border-0 shadow-lg my-5">
                            <div className="card-body p-0">
                                <div className="row">
                                    <div className="col">
                                        <div className="p-5">
                                            <div className="text-center">
                                                <h1 className="h4 text-gray-900 mb-4">
                                                    Tu dois te connecter
                                                </h1>
                                            </div>
                                            <form className="user">
                                                <a href="http://localhost:50451/api/discord/login" className="btn btn-discord btn-user btn-block">
                                                    <i className="fab fa-discord fa-fw" />
                                                    Connexion avec Discord
                                                </a>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>

            </div>
        );
    }

}

export default Login;