import React from "react";
import Api from "../Api";
import {Redirect, withRouter} from "react-router-dom";
import {SortableContainer, SortableElement, SortableHandle} from "react-sortable-hoc";
import arrayMove from "array-move";

class EditPlaylist extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            inputName: '',
            inputPublic: false,
            songs: [],
            redirect: false
        }
        //alert(this.props.match.params.id)
    }

    async componentDidMount() {
        this.setState({redirect: false});
        const playlistId = this.props.match.params.id;
        const userId = this.props.app.user.id;
        const serverId = this.props.app.server;
        const result = await Api.postBot('/api/music/playlist', userId, serverId, [playlistId]);
        if (result.code === 200) {
            const inputName = result.playlist.title;
            const inputPublic = result.playlist.public;
            this.setState({inputName, inputPublic});
        }
        const result2 = await Api.postBot('/api/music/playlist/songs', userId, serverId, [playlistId]);
        if (result2.code === 200) {
            const songs = result2.songs;
            this.setState({songs});
        }
    }

    editPlaylistHandler = async (e) => {
        e.preventDefault();
        const userId = this.props.app.user.id;
        const serverId = this.props.app.server;
        const result = await Api.postBot('/api/music/playlist_edit', userId, serverId, [this.state.inputName, this.state.inputPublic, this.props.match.params.id]);
        if (result['code'] === 200) {
        }
    }

    deletePlaylistHandler = async (e) => {
        e.preventDefault();
        const userId = this.props.app.user.id;
        const serverId = this.props.app.server;
        const result = await Api.postBot('/api/music/playlist/delete', userId, serverId, [this.props.match.params.id]);
        if (result['code'] === 200) {
            this.setState({redirect: true});
        }
    }

    onSortEnd = async ({oldIndex, newIndex}) => {
        const playlistId = this.props.match.params.id;
        const songs = arrayMove(this.state.songs, oldIndex, newIndex);
        this.setState({songs});
        const userId = this.props.app.user.id;
        const serverId = this.props.app.server;
        await Api.postBot('/api/music/playlist/save', userId, serverId, [playlistId, songs]);
    };

    handleAddToPlaylist = async (playlistId, song) => {
        //alert(playlistId + '   ' + song.title)
        const userId = this.props.app.user.id;
        const serverId = this.props.app.server;
        await Api.postBot('/api/music/playlist_add_songs', userId, serverId, [playlistId, [song]]);
    }

    handlePlay = async (url) => {
        const userId = this.props.app.user.id;
        const serverId = this.props.app.server;
        await Api.postBot('/api/music/play_song', userId, serverId, [url]);
    }

    handleDelete = async (index) => {
        const userId = this.props.app.user.id;
        const serverId = this.props.app.server;
        const result = await Api.postBot('/api/music/playlist/deleteSong', userId, serverId, [this.props.match.params.id, index]);
        if(result.code === 200) {
            const songs = [];
            this.state.songs.forEach((song, songIndex) => {
                if(index !== songIndex) {
                    songs.push(song);
                }
            });
            this.setState({songs});
        }
    }

    handleAddSongToQueue = async (url) => {
        const userId = this.props.app.user.id;
        const serverId = this.props.app.server;
        await Api.postBot('/api/music/queue_add_song', userId, serverId, [url]);
    }

    handleNameChange = (e) => this.setState({inputName: e.target.value});
    handlePublicChange = (e) => this.setState({inputPublic: e.target.checked});

    render() {

        const playButton = (url) => <button onClick={() => this.handlePlay(url)}
                                            className="wm-not-button fa fa-play text-gray-200"/>;

        const addButton = (song, key) => <div className="dropdown d-inline">
            <button className="wm-not-button fa fa-plus text-success" id={"addTo" + key}
                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"/>
            <div className="dropdown-menu dropdown-menu-right shadow animated--fade-in "
                 aria-labelledby={"addTo" + key}>
                {this.props.app.playing && <button className="dropdown-item"
                                                   onClick={() => this.handleAddSongToQueue(song.url)}>
                    <i className="fas fa-list fa-sm fa-fw mr-2 text-gray-400"/>
                    File d'attente
                </button>}
                {this.props.app.ownedPlaylists.map(playlist => {
                    return <button onClick={() => this.handleAddToPlaylist(playlist.id, song)}
                                   className="dropdown-item">
                        <i className="fas fa-compact-disc fa-sm fa-fw mr-2 text-gray-400"/>
                        {playlist.title}
                    </button>
                })}
            </div>
        </div>

        const deleteButton = (index) => <button onClick={() => this.handleDelete(index)}
                                            className="wm-not-button fa fa-trash text-danger"/>;

        const DragHandle = SortableHandle(() => <span><i className="fa fa-bars text-primary"/></span>);

        const SortableItem = SortableElement(({value}) => {
            const key = value.key;
            const song = value.song;
            const index = value.index;
            return <tr id={key}>
                <td><DragHandle/></td>
                <td>{song.duration}</td>
                <td>{song.artist} - {song.title}</td>
                <td className="wm-table-buttons wm-sort-disabled">
                    {playButton(song.url)}
                    {addButton(song, key)}
                    {deleteButton(index)}
                </td>
            </tr>
        });

        const SortableList = SortableContainer(({items}) => {
            return (
                <table className={"table table-dark table-hover"}>
                    <tbody>
                    {items.map((song, index) => {
                        const key = 'x' + Math.floor(Math.random() * 1000000000);
                        return <SortableItem index={index} key={key} value={{song, key, index}}/>
                    })}
                    </tbody>
                </table>
            );
        });

        return (<>
            {this.state.redirect && <Redirect to={"/playlists"}/>}
            <h1 className="h3 text-gray-300 mb-4 ml-4">Modifier une playlist</h1>
            <form className="m-4">
                <div className="mb-3">
                    <label htmlFor="name">Nom de la playlist :</label>
                    <input onChange={this.handleNameChange} value={this.state.inputName} type="text" id="name"
                           placeholder="Ma super playlist" className="form-control bg-dark border-0 text-gray-200"/>
                </div>
                <div className="mb-3 form-check ml-1">
                    <input onChange={this.handlePublicChange} checked={this.state.inputPublic} type="checkbox"
                           className="form-check-input" id="exampleCheck1"/>
                    <label className="form-check-label" htmlFor="exampleCheck1">Publique</label>
                </div>
                <button onClick={this.editPlaylistHandler} type="submit" className="btn btn-warning btn-block">Modifier
                </button>
                <button onClick={this.deletePlaylistHandler} className="btn btn-danger btn-block">Supprimer
                </button>
            </form>
            <p className="mb-4 ml-4">
                Pour ajouter des musiques dans ta playlist, effectue une recherche.<br/>
                Tu pourras ensuite ajouter les résultats à ta playlist avec l'icône plus.
            </p>
            <SortableList helperClass={"wm-test-sort"} lockAxis={"y"} items={this.state.songs}
                          onSortEnd={this.onSortEnd} useDragHandle/>
        </>);
    }

}

export default withRouter(EditPlaylist);