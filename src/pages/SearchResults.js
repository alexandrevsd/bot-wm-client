import React from "react";
import {withRouter} from 'react-router-dom'
import Utils from "../Utils";
import SongsResult from "../components/SongsResult";
import PlaylistsResult from "../components/PlaylistsResult";
import Api from "../Api";

class SearchResults extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        const urlPlaylistTitle = this.props.location.state.urlPlaylist !== undefined ? `Playlist ${Utils.capitalize(this.props.location.state.urlPlaylist.service)} "${this.props.location.state.urlPlaylist.title} trouvée` : undefined;

        return (
            <>
                <div style={{marginTop: '-25px'}}></div>
                <table style={{marginTop: '-16px'}} className={"table table-dark table-hover"}>
                    {this.props.location.state.urlList.length > 0 &&
                    <SongsResult title={"Musique trouvée par URL"} list={this.props.location.state.urlList}
                                 app={this.props.app}/>}
                    {this.props.location.state.urlPlaylist !== undefined &&
                    <SongsResult title={urlPlaylistTitle} list={this.props.location.state.urlPlaylist.songs}
                                 app={this.props.app}/>}
                    {this.props.location.state.playlistsList.length > 0 &&
                    <PlaylistsResult title={"Playlists enregistrées trouvées"}
                                     list={this.props.location.state.playlistsList}
                                     app={this.props.app}/>}
                    {this.props.location.state.youtubeList.length > 0 &&
                    <SongsResult title={"Résultats de recherche sur Youtube"}
                                 list={this.props.location.state.youtubeList}
                                 app={this.props.app}/>}
                    {this.props.location.state.soundcloudList.length > 0 &&
                    <SongsResult title={"Résultats de recherche sur Soundcloud"}
                                 list={this.props.location.state.soundcloudList}
                                 app={this.props.app}/>}
                </table>
            </>
        );
    }

}

export default withRouter(SearchResults);