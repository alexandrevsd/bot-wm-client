import React from "react";
import Api from "../Api";
import {withRouter} from "react-router-dom";
import {SortableContainer, SortableElement, SortableHandle} from "react-sortable-hoc";
import arrayMove from "array-move";
import Utils from "../Utils";

class SeePlaylist extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            playlist: {
                title: "Loading"
            },
            songs: []
        }
        //alert(this.props.match.params.id)
    }

    async componentDidMount() {
        const playlistId = this.props.match.params.id;
        const userId = this.props.app.user.id;
        const serverId = this.props.app.server;
        const result = await Api.postBot('/api/music/playlist', userId, serverId, [playlistId]);
        if (result.code === 200) {
            const playlist = result.playlist;
            this.setState({playlist});
        }
        const result2 = await Api.postBot('/api/music/playlist/songs', userId, serverId, [playlistId]);
        if (result2.code === 200) {
            const songs = result2.songs;
            this.setState({songs});
        }
    }


    handleAddToPlaylist = async (playlistId, song) => {
        //alert(playlistId + '   ' + song.title)
        const userId = this.props.app.user.id;
        const serverId = this.props.app.server;
        await Api.postBot('/api/music/playlist_add_songs', userId, serverId, [playlistId, [song]]);
    }

    handlePlay = async (url) => {
        const userId = this.props.app.user.id;
        const serverId = this.props.app.server;
        await Api.postBot('/api/music/play_song', userId, serverId, [url]);
    }

    handleAddSongToQueue = async (url) => {
        const userId = this.props.app.user.id;
        const serverId = this.props.app.server;
        await Api.postBot('/api/music/queue_add_song', userId, serverId, [url]);
    }
    render() {

        const playButton = (url) => <button onClick={() => this.handlePlay(url)}
                                             className="wm-not-button fa fa-play text-gray-200"/>;

        const addButton = (song, key) => <div className="dropdown d-inline">
            <button className="wm-not-button fa fa-plus text-success" id={"addTo" + key}
                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"/>
            <div className="dropdown-menu dropdown-menu-right shadow animated--fade-in "
                 aria-labelledby={"addTo" + key}>
                {this.props.app.playing && <button className="dropdown-item"
                                                   onClick={() => this.handleAddSongToQueue(song.url)}>
                    <i className="fas fa-list fa-sm fa-fw mr-2 text-gray-400"/>
                    File d'attente
                </button>}
                {this.props.app.ownedPlaylists.map(playlist => {
                    return <button onClick={() => this.handleAddToPlaylist(playlist.id, song)}
                                   className="dropdown-item">
                        <i className="fas fa-compact-disc fa-sm fa-fw mr-2 text-gray-400"/>
                        {playlist.title}
                    </button>
                })}
            </div>
        </div>

        return (<>
            <h1 className="h3 text-gray-300 mb-4 ml-4">Playlist "{this.state.playlist.title}" de {this.state.playlist.ownerUsername}</h1>
            <table className="table table-hover table-dark">
                <tbody>
                {this.state.songs.map(song => {
                    const key = Utils.randomKey();
                    return <tr key={key}>
                        <td></td>
                        <td>{song.duration}</td>
                        <td>{song.artist} - {song.title}</td>
                        <td className="wm-table-buttons wm-sort-disabled">
                            {playButton(song.url)}
                            {addButton(song, key)}
                        </td>
                    </tr>;
                })}
                </tbody>
            </table>
        </>);
    }

}

export default withRouter(SeePlaylist);