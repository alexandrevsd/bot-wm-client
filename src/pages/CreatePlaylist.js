import React from "react";
import Api from "../Api";

class CreatePlaylist extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            inputName: '',
            inputPublic: false
        }
    }

    handleSubmit = async (e) => {
        e.preventDefault();
        const userId = this.props.app.user.id;
        const serverId = this.props.app.server;
        const result = await Api.postBot('/api/music/playlist_edit', userId, serverId, [this.state.inputName, this.state.inputPublic]);
        if(result['code'] === 200) {
            alert('ok')
            console.log(result.playlist);
        } else {
            alert('erreur')
        }
    }

    handleNameChange = (e) => this.setState({inputName: e.target.value});
    handlePublicChange = (e) => this.setState({inputPublic: e.target.checked});

    render() {
        return (<>
            <h1 className="h3 text-gray-300 mb-4 ml-4">Créer une playlist</h1>
            <form className="m-4">
                <div className="mb-3">
                    <label htmlFor="name">Nom de la playlist :</label>
                    <input onChange={this.handleNameChange} value={this.state.inputName} type="text" id="name" placeholder="Ma super playlist" className="form-control bg-dark border-0 text-gray-200" />
                </div>
                <div className="mb-3 form-check ml-1">
                    <input onChange={this.handlePublicChange} checked={this.state.inputPublic} type="checkbox" className="form-check-input" id="exampleCheck1" />
                    <label className="form-check-label" htmlFor="exampleCheck1">Publique</label>
                </div>
                <button onClick={this.handleSubmit} type="submit" className="btn btn-success btn-block">Créer</button>
            </form>
        </>);
    }

}

export default CreatePlaylist;