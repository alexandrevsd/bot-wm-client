import React from "react";
import PageContainer from "../components/PageContainer";
import Api from "../Api";
import Utils from "../Utils";

class Lyrics extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            lyrics: 'Chargement des paroles...',
            title: 'Paroles'
        }
    }

    async componentDidMount() {
        const userId = this.props.app.user.id;
        const serverId = this.props.app.server;
        const result = await Api.postBot('/api/music/lyrics', userId, serverId);
        if (result.code === 200) {
            const jsonLyrics = JSON.stringify(result.lyrics);
            const notJsonLyrics = jsonLyrics.substr(1, jsonLyrics.length - 2).replaceAll('\\"', '"');
            const lyrics = Utils.convertHTMLEntity(notJsonLyrics);
            this.setState({lyrics, title: `Paroles de ${result.song.title}`});
        } else {
            this.setState({lyrics: 'Paroles introuvables !'})
        }
    }

    render() {
        return (
            <>
            <PageContainer title={this.state.title}>
                {this.state.lyrics.split('\\n').map((it, i) => <>{it}<br /></>)}
            </PageContainer>
                <><div className={"d-sm-none d-md-block"} style={{marginTop:'134px'}} /><div className={"d-sm-block d-md-none"} style={{marginTop:'235px'}} /></>
            </>
        );
    }

}

export default Lyrics;