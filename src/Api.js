const BOT_HTTP_SERVER_URL = 'http://localhost:8080';


class Api {

    static async get(url) {
        return await (await fetch(url)).json();
    }

    static async postBot(url, userId, serverId, args = []) {
        return await (await fetch(BOT_HTTP_SERVER_URL + url, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({serverId, userId, args})
        })).json();
    }

}

export default Api;