import React from "react";
import {Switch, Route} from "react-router-dom";
import "bootstrap/dist/js/bootstrap.min";
import "./sb-admin-2.min.css";
import "./custom.css";
import MainWrapper from "./components/MainWrapper";
import SideBar from "./components/SideBar";
import Menu from "./components/Menu";
import TopBar from "./components/TopBar";
import ContentWrapper from "./components/ContentWrapper";
import Music from "./pages/Music";
import Lyrics from "./pages/Lyrics";
import Playlists from "./pages/Playlists";
import CreatePlaylist from "./pages/CreatePlaylist";
import Player from "./components/Player";
import Loading from "./pages/Loading";
import Login from "./pages/Login";
import Api from "./Api";
import Server from "./pages/Server";
import Modal from "./components/Modal";
import SearchResults from "./pages/SearchResults";
import Socket from "./Socket";
import EditPlaylist from "./pages/EditPlaylist";
import SeePlaylist from "./pages/SeePlaylist";

class App extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            playing: false,
            loading: true,
            user: undefined,
            server: undefined,
            resetLyrics: false,
            modal: {
                title: "Test",
                body: "Hello",
                buttons: "ok"
            },
            urlList: undefined,
            playlistList: undefined,
            youtubeList: undefined,
            soundcloudList: undefined,
            ownedPlaylists: []
        }
    }

    async componentDidMount() {
        const result = await Api.get('/api/discord/logged');
        if (result.code === 200) {
            this.setState({user: result.user})
            const userId = result.user.id;
            const result2 = await Api.postBot('/api/misc/search_user', userId);
            if(result2.code === 200) {
                this.setState({server: result2.server});
            }
        }
        this.setState({loading: false});
        Socket.getSocket().on('channel_left', ({userId}) => {
            if(this.state.user) {
                if(userId === this.state.user.id) {
                    this.setState({server: undefined})
                }
            }
        });
        if(this.state.user) {
            const userId = this.state.user.id;
            const serverId = this.state.server;
            const result2 = await Api.postBot('/api/music/playlists', userId, serverId, ['owned']);
            this.setState({ownedPlaylists: result2.playlists});
        }
    }

    setAppState = (state) => this.setState(state);

    logout = () => {
        this.setState({
            user: undefined,
            server: undefined
        })
    }

    getApp() {
        return {
            ownedPlaylists: this.state.ownedPlaylists,
            playing: this.state.playing,
            user: this.state.user,
            server: this.state.server,
            resetLyrics: this.state.resetLyrics,
            urlList: this.state.urlList,
            urlPlaylist: this.state.urlPlaylist,
            playlistList: this.state.playlistList,
            youtubeList: this.state.youtubeList,
            soundcloudList: this.state.soundcloudList,
            setState: this.setAppState,
            logout: this.logout
        }
    }


    render() {
        return (<>
                {this.state.loading ?
                    <Loading/>
                    :
                    this.state.user === undefined ?
                        <Login/>
                        :
                        this.state.server === undefined ?
                            <Server app={this.getApp()}/>
                            :
                            <>
                                <MainWrapper>
                                    <SideBar>
                                        <Menu/>
                                    </SideBar>
                                    <ContentWrapper>
                                        <TopBar app={this.getApp()}/>
                                        <Switch>
                                            <Route exact path={"/"} component={() => <Music app={this.getApp()} />}/>
                                            <Route path={"/lyrics"} component={() => <Lyrics app={this.getApp()} />}/>
                                            <Route path={"/results"} component={() => <SearchResults app={this.getApp()} />}/>
                                            <Route exact path={"/playlists"} component={() => <Playlists app={this.getApp()} />}/>
                                            <Route exact path={"/playlists/create"} component={() => <CreatePlaylist app={this.getApp()} />}/>
                                            <Route exact path={"/playlists/edit/:id"} component={() => <EditPlaylist app={this.getApp()} />}/>
                                            <Route exact path={"/playlists/see/:id"} component={() => <SeePlaylist app={this.getApp()} />}/>
                                        </Switch>
                                        {this.state.playing && <>
                                            <div className={"d-sm-none d-md-block"} style={{marginTop: '127px'}}/>
                                            <div className={"d-sm-block d-md-none"} style={{marginTop: '229px'}}/>
                                        </>}
                                    </ContentWrapper>
                                    <Player app={this.getApp()}/>
                                </MainWrapper>
                            </>}
                <Modal title={this.state.modal.title} body={this.state.modal.body} buttons={this.state.modal.buttons}/>
            </>
        );
    }

}

export default App;