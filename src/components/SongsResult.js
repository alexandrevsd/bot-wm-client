import React from "react";
import Api from "../Api";

const SongsResult = (props) => {

    const userId = props.app.user.id;
    const serverId = props.app.server;

    const handleAddToPlaylist = async (playlistId, song) => {
        //alert(playlistId + '   ' + song.title)
        await Api.postBot('/api/music/playlist_add_songs', userId, serverId, [playlistId, [song]]);
    }

    const handlePlay = async (url, trId) => {
        await Api.postBot('/api/music/play_song', userId, serverId, [url]);
    }

    const handleAddToQueue = async (url, trId) => {
        await Api.postBot('/api/music/queue_add_song', userId, serverId, [url]);
        const tr = document.getElementById(trId);
        tr.classList.add('wm-added-row');
    }

    const handleAddSongsToQueue = async (url, trId) => {
        await Api.postBot('/api/music/add_songs', userId, serverId, [props.list]);
    }

    return (
        <>
            <thead>
            <tr>
                <th colSpan="100%">{props.title} {
                    props.app.playing ?
                        <button className="btn btn-success btn-sm wm-right" onClick={handleAddSongsToQueue}>Tout
                            ajouter</button>
                        :
                        <button className="btn btn-light btn-sm wm-right" onClick={handleAddSongsToQueue}>Tout
                            écouter</button>
                }</th>
            </tr>
            </thead>
            <tbody>
            {props.list.map(song => {
                const serviceImage = song.service === 'youtube' ?
                    <i className="fab fa-youtube" style={{color: "red"}}/> :
                    song.service === 'soundcloud' ? <i className="fab fa-soundcloud" style={{color: "orange"}}/> :
                        <i className="fa fa-robot text-primary"/>;
                const trId = 'x' + Math.floor(Math.random() * 1000000000);
                return (
                    <tr id={trId} key={trId}>
                        <td>{serviceImage}</td>
                        <td>{song.duration}</td>
                        <td>{`${song.artist} - `}{song.title}</td>
                        <td className="wm-table-buttons">
                            <button onClick={() => handlePlay(song.url, trId)}
                                    className="wm-not-button fa fa-play text-gray-200"/>
                            {(props.app.ownedPlaylists.length > 0 || props.app.playing) &&
                            <div className="dropdown d-inline">
                                <button className="wm-not-button fa fa-plus text-success" id={"addTo" + trId}
                                        role="button"
                                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"/>
                                <div className="dropdown-menu dropdown-menu-right shadow animated--fade-in "
                                     aria-labelledby={"addTo" + trId}>
                                    {props.app.playing &&
                                    <button className="dropdown-item" onClick={() => handleAddToQueue(song.url, trId)}>
                                        <i className="fas fa-list fa-sm fa-fw mr-2 text-gray-400"/>
                                        File d'attente
                                    </button>}
                                    {props.app.ownedPlaylists.map(playlist => {
                                        return <button onClick={() => handleAddToPlaylist(playlist.id, song)}
                                                       className="dropdown-item">
                                            <i className="fas fa-compact-disc fa-sm fa-fw mr-2 text-gray-400"/>
                                            {playlist.title}
                                        </button>
                                    })}
                                </div>
                            </div>
                            }

                        </td>
                    </tr>
                );
            })}
            </tbody>
        </>
    );
};

export default SongsResult;