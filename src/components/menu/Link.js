import {NavLink} from "react-router-dom";

const Link = (props) => {
    const exact = props.exact || false;
    return (
        <NavLink exact={exact} to={props.to} className="nav-item">
            <span className="nav-link">
                <i className={"fas fa-fw fa-" + props.icon}/>
                <span>{props.title}</span>
            </span>
        </NavLink>
    );
}

export default Link;