import {NavLink} from "react-router-dom";

const GroupLink = (props) => <NavLink className="collapse-item" to={props.to}>{props.title}</NavLink>;

export default GroupLink;