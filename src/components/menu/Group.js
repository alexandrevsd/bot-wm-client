import { useLocation } from "react-router-dom";

const Group = (props) => {
    const active = useLocation().pathname.startsWith(props.to + "/") ? " active" : "";
    return (
        <li className={"nav-item" + active}>
            <a className="nav-link collapsed" href={"#collapse-" + props.icon} data-toggle="collapse"
               data-target={"#collapse-" + props.icon}
               aria-expanded="true" aria-controls={"collapse-" + props.icon}>
                <i className={"fas fa-fw fa-" + props.icon}/>
                <span>Paramètres</span>
            </a>

            <div id={"collapse-" + props.icon} className="collapse" data-parent="#accordionSidebar">
                <div className="bg-white py-2 collapse-inner rounded">
                    {props.children}
                </div>
            </div>
        </li>
    );
}

export default Group;
