import Title from "./sideBar/Title";

const SideBar = (props) => <ul className="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion"
                               id="accordionSidebar">
    <Title>Visdi</Title>
    <hr className="sidebar-divider my-0" />
    {props.children}
    <hr className="sidebar-divider d-none d-md-block" />
    <div className="text-center d-none d-md-inline">
        <button className="rounded-circle border-0" id="sidebarToggle" />
    </div>
</ul>;
export default SideBar;