import React from "react";

class MainWrapper extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        return <div id="wrapper">{this.props.children}</div>;
    }
}
export default MainWrapper;