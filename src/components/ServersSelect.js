import React from "react";
import {Redirect} from "react-router-dom";


class ServersSelect extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            redirect: false
        }
    }

    handleServerChange = (e) => {
        e.preventDefault();
        e.target.value !== "" && this.props.app.setState({server: e.target.value});
        this.setState({redirect: true});
    }

    render() {
        return (
            <>
                {this.state.redirect && <Redirect to={"/"}/>}
                <select className="form-select" onChange={this.handleServerChange}>
                    <option value="">Sélectionnes un serveur</option>
                    {this.props.app.servers.map(server => <option value={server.id}>{server.name}</option>)}
                </select>
            </>
        );
    }

}

export default ServersSelect;