import React from "react";
import Api from "../../Api";
import {Redirect} from "react-router-dom";
import $ from "jquery";

class Search extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            queryInput: '',
            searchButtonDisabled: false,
            searchInputDisabled: false,
            urlList: undefined,
            urlPlaylist: undefined,
            playlistsList: undefined,
            youtubeList: undefined,
            soundcloudList: undefined,
            redirect: false
        }
    }


    handleSearchClick = async (e) => {
        e.preventDefault();
        this.setState({searchButtonDisabled: true, searchInputDisabled: true});
        const userId = this.props.app.user.id;
        const serverId = this.props.app.server;
        const args = this.state.queryInput.split(' ');
        this.setState({queryInput: ''});
        const result = await Api.postBot('/api/music/retrieve', userId, serverId, args);
        if (result.code !== 200 || (result.urlList.length === 0 && result.urlPlaylist === undefined && result.playlistsList.length === 0 && result.youtubeList.length === 0 && result.soundcloudList.length === 0)) {
            this.props.app.setState({
                modal: {
                    title: "Pas de résultats",
                    body: <>
                        Aucun résultat avec la recherche effectuée
                    </>,
                    buttons: <button className="btn btn-secondary" data-dismiss="modal">Fermer</button>
                }
            })
            $('#modal').modal();
        } else {
            this.setState({
                urlList: result.urlList,
                urlPlaylist: result.urlPlaylist,
                playlistsList: result.playlistsList,
                youtubeList: result.youtubeList,
                soundcloudList: result.soundcloudList,
                redirect: true
            });
        }
        this.setState({searchButtonDisabled: false, searchInputDisabled: false});
    }

    handleSearchChange = (e) => this.setState({queryInput: e.target.value});

    render() {
        return (
            <form className="d-inline-block form-inline my-2 w-100 navbar-search">
                {this.state.redirect && <Redirect to={{
                    pathname: '/results',
                    state: {
                        urlList: this.state.urlList,
                        urlPlaylist: this.state.urlPlaylist,
                        playlistsList: this.state.playlistsList,
                        youtubeList: this.state.youtubeList,
                        soundcloudList: this.state.soundcloudList
                    }
                }}/>}
                <div className="input-group">
                    <input disabled={this.state.searchInputDisabled} value={this.state.queryInput}
                           onChange={this.handleSearchChange} type="text"
                           className="form-control bg-dark text-gray-200 border-0 small"
                           placeholder="Nom de playlist, mots-clés ou URL"
                           aria-label="Search" aria-describedby="basic-addon2"/>
                    <div className="input-group-append">
                        <button className="btn btn-primary" disabled={this.state.searchButtonDisabled}
                                onClick={this.handleSearchClick} type="submit">
                            <i className="fas fa-search fa-sm"/>
                        </button>
                    </div>
                </div>
            </form>
        );
    }
}

export default Search;