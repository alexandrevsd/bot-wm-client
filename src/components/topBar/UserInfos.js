import React from "react";
import $ from "jquery";
import Utils from "../../Utils";

class UserInfos extends React.Component {

    constructor(props) {
        super(props);
    }


    componentDidMount() {
        this.loadJQueryForTheme();
    }

    handleLogoutClick = () => {
        const title = "Déconnexion";
        const body = <>Veux-tu vraiment te déconnecter ?</>;
        const buttons = <>
            <button className="btn btn-danger" data-dismiss="modal">Non</button>
            <button onClick={this.props.app.logout} className="btn btn-success" data-dismiss="modal">Oui</button>
        </>;
        Utils.modal(title, body, buttons, this.props.app);
    }

    render() {
        return (
            <ul className="navbar-nav ml-auto">
                <li className="nav-item dropdown no-arrow">
                    <a className="nav-link dropdown-toggle" href="#" id="userDropdown" role="button"
                       data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span
                            className="mr-2 d-none d-lg-inline text-gray-300 small">{this.props.app.user.username}</span>
                        <img className="img-profile rounded-circle"
                             src={`https://cdn.discordapp.com/avatars/${this.props.app.user.id}/${this.props.app.user.avatar}.png`}/>
                    </a>
                    <div className="dropdown-menu dropdown-menu-right shadow animated--grow-in"
                         aria-labelledby="userDropdown">
                        <button className="dropdown-item" onClick={this.handleLogoutClick}>
                            <i className="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"/>
                            Déconnexion
                        </button>
                    </div>
                </li>
            </ul>
        );
    }

    loadJQueryForTheme() {
        $("#sidebarToggle, #sidebarToggleTop").on('click', function (e) {
            $("body").toggleClass("sidebar-toggled");
            $(".sidebar").toggleClass("toggled");
            if ($(".sidebar").hasClass("toggled")) {
                $('.sidebar .collapse').collapse('hide');
            }
            ;
        });

        $(window).resize(function () {
            if ($(window).width() < 768) {
                $('.sidebar .collapse').collapse('hide');
            }
            ;

            if ($(window).width() < 480 && !$(".sidebar").hasClass("toggled")) {
                $("body").addClass("sidebar-toggled");
                $(".sidebar").addClass("toggled");
                $('.sidebar .collapse').collapse('hide');
            }
            ;
        });

        $('body.fixed-nav .sidebar').on('mousewheel DOMMouseScroll wheel', function (e) {
            if ($(window).width() > 768) {
                let e0 = e.originalEvent,
                    delta = e0.wheelDelta || -e0.detail;
                this.scrollTop += (delta < 0 ? 1 : -1) * 30;
                e.preventDefault();
            }
        });

        $(document).on('scroll', function () {
            let scrollDistance = $(this).scrollTop();
            if (scrollDistance > 100) {
                $('.scroll-to-top').fadeIn();
            } else {
                $('.scroll-to-top').fadeOut();
            }
        });

        $(document).on('click', 'a.scroll-to-top', function (e) {
            let $anchor = $(this);
            $('html, body').stop().animate({
                scrollTop: ($($anchor.attr('href')).offset().top)
            }, 1000, 'easeInOutExpo');
            e.preventDefault();
        });
    }
}

export default UserInfos;