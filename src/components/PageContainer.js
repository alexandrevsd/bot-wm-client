const PageContainer = (props) => <div className="container-fluid">
    <div className="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 className="h3 mb-0 text-gray-300">{props.title}</h1>
    </div>
    {props.children}
</div>;
export default PageContainer;