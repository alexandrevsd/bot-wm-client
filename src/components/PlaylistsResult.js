import Api from "../Api";
import React from "react";
import Utils from "../Utils";

const PlaylistsResult = (props) => {

    const handlePlay = async (playlistId) => {
        const userId = props.app.user.id;
        const serverId = props.app.server;
        console.log(await Api.postBot('/api/music/playlist/play', userId, serverId, [playlistId]));
    }

    const handleAddToQueue = async (playlistId) => {
        const userId = props.app.user.id;
        const serverId = props.app.server;
        console.log(await Api.postBot('/api/music/queue/addPlaylist', userId, serverId, [playlistId]));
    }

    const handleAddToPlaylist = async (fromPlaylistId, toPlaylistId) => {
        const userId = props.app.user.id;
        const serverId = props.app.server;
        console.log(await Api.postBot('/api/music/playlist/addPlaylist', userId, serverId, [fromPlaylistId, toPlaylistId]));
    }

    return (
        <>
            <thead>
            <tr>
                <th colSpan="4">{props.title}</th>
            </tr>
            </thead>
            <tbody>
            {props.list.map(playlist => {
                return (
                    <tr>
                        <td><i className="fa fa-robot text-primary"/></td>
                        <td>{Utils.getSongDuration(playlist.length)}</td>
                        <td>{playlist.title}</td>
                        <td className="wm-table-buttons">
                            <button onClick={() => handlePlay(playlist.id)}
                                    className="wm-not-button fa fa-play text-gray-200"/>
                            {(props.app.ownedPlaylists.length > 0 || props.app.playing) &&
                            <div className="dropdown d-inline">
                                <button className="wm-not-button fa fa-plus text-success" id={"addTo"}
                                        role="button"
                                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"/>
                                <div className="dropdown-menu dropdown-menu-right shadow animated--fade-in "
                                     aria-labelledby={"addTo"}>
                                    {props.app.playing &&
                                    <button className="dropdown-item" onClick={() => handleAddToQueue(playlist.id)}>
                                        <i className="fas fa-list fa-sm fa-fw mr-2 text-gray-400"/>
                                        File d'attente
                                    </button>}
                                    {props.app.ownedPlaylists.map(ownedPlaylist => {
                                        return <button onClick={() => handleAddToPlaylist(playlist.id, ownedPlaylist.id)} className="dropdown-item">
                                            <i className="fas fa-compact-disc fa-sm fa-fw mr-2 text-gray-400"/>
                                            {ownedPlaylist.title}
                                        </button>
                                    })}
                                </div>
                            </div>
                            }

                        </td>
                    </tr>
                );
            })}
            </tbody>
        </>
    );
};

export default PlaylistsResult;