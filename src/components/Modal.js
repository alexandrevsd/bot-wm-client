const Modal = (props) => <div className="modal fade" id="modal" tabIndex="-1" role="dialog" aria-hidden="true">
    <div className="modal-dialog" role="document">
        <div className="modal-content">
            <div className="modal-header">
                <h5 className="modal-title" id="exampleModalLabel">{props.title}</h5>
                <button className="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div className="modal-body">{props.body}</div>
            <div className="modal-footer">
                {props.buttons}
            </div>
        </div>
    </div>
</div>;
export default Modal;