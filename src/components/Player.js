import React from "react";
import Socket from "../Socket";
import Utils from "../Utils";
import {withRouter} from "react-router-dom";
import Api from "../Api";

class Player extends React.Component {

    timeCalculatingInterval;

    constructor(props) {
        super(props);
        this.state = {
            playPauseButtonDisabled: false,
            previousButtonDisabled: false,
            nextButtonDisabled: false,
            randomButtonDisabled: false,
            loopButtonDisabled: false,
            intervalEnabled: false,
            changingTime: false,
            seeking: false,
            timeChangingDisabled: false,
            paused: false,
            startedAt: 0,
            elapsedTime: 0,
            length: 0,
            duration: '00:00',
            loop: false,
            random: false,
            song: {
                artist: "Not playing",
                title: "Not playing",
                image: "",
                length: 0,
                service: "youtube",
                url: ""
            }
        }
    }

    componentDidMount() {
        const serverId = this.props.app.server;

        // Socket event listeners

        Socket.getSocket().emit('getPlayer', serverId);
        Socket.getSocket().on(serverId + 'player', ({paused, elapsedTime, playing, length, song, random, loop}) => {
            this.props.app.setState({playing});
            this.setState({
                paused,
                elapsedTime,
                length,
                seeking: false,
                timeChangingDisabled: false,
                changingTime: false,
                duration: Utils.getSongDuration(length),
                random,
                loop,
                song
            });
            if (!this.state.paused) this.startTimeCalculating();
        });

        Socket.getSocket().on(serverId + 'song', ({song, elapsedTime}) => {
            this.props.app.setState({playing: true});
            this.setState({
                startedAt: Date.now(),
                paused: false,
                seeking: false,
                timeChangingDisabled: false,
                changingTime: false,
                elapsedTime,
                song
            });
            this.startTimeCalculating();
        });

        Socket.getSocket().on(serverId + 'stop', () => {
            this.props.app.setState({playing: false});
            this.stopTimeCalculating();
        });

        Socket.getSocket().on(serverId + 'pause', () => {
            this.setState({paused: true});
            this.stopTimeCalculating();
        });

        Socket.getSocket().on(serverId + 'resume', (elapsedTime) => {
            this.setState({paused: false, startedAt: Date.now(), elapsedTime});
            this.startTimeCalculating();
        });

        Socket.getSocket().on('disconnect', () => {
            this.props.app.setState({playing: false});
            this.stopTimeCalculating();
        });

        //TODO On disconnect pour le socket

    }

    // Time calculating
    startTimeCalculating = () => {
        if (!this.state.intervalEnabled) {
            this.timeCalculatingInterval = setInterval(this.songTimeInterval, 50);
            this.setState({intervalEnabled: true});
        }
    }
    stopTimeCalculating = () => {
        clearInterval(this.timeCalculatingInterval);
        this.setState({intervalEnabled: false});
    }
    songTimeInterval = () => {
        if (this.props.app.playing && !this.state.paused && !this.state.seeking) {
            if (this.state.startedAt === 0 || this.state.startedAt === undefined) {
                this.setState({startedAt: Date.now()})
            }

            const elapsed = Date.now() - this.state.startedAt + this.state.elapsedTime;
            if (!this.state.changingTime) {
                this.setState({length: Math.floor(elapsed)});
            }
            this.setState({duration: Utils.getSongDuration(Math.round(elapsed))});
        }
    }

    // Time changing
    handleTimeChange = (e) => this.setState({length: e.target.value});
    handleChangingTime = () => this.setState({changingTime: true});
    handleStopChangingTime = async (e) => {
        this.setState({timeChangingDisabled: true, seeking: true});
        const userId = this.props.app.user.id;
        const serverId = this.props.app.server;
        const toSeek = (this.state.length - Utils.getSongLength('formatted', this.state.duration)) / 1000;
        const result = await Api.postBot('/api/music/seek', userId, serverId, [toSeek]);
        if (result.code === 200) {

        }
    }

    handleLyricsClick = () => {
        this.props.app.setState({resetLyrics: true});
        this.props.history.push('/lyrics');
    }

    handleBotCommand = async (command) => {
        const userId = this.props.app.user.id;
        const serverId = this.props.app.server;
        const result = await Api.postBot('/api/music/' + command, userId, serverId);
    }

    handlePlayPause = async () => {
        this.setState({playPauseButtonDisabled: true});
        const action = this.state.paused ? 'resume' : 'pause';
        await this.handleBotCommand(action);
        this.setState({playPauseButtonDisabled: false});
    }

    handlePrevious = async () => {
        this.setState({previousButtonDisabled: true});
        await this.handleBotCommand('previous');
        this.setState({previousButtonDisabled: false});
    }

    handleNext = async () => {
        this.setState({nextButtonDisabled: true});
        await this.handleBotCommand('next');
        this.setState({nextButtonDisabled: false});
    }

    handleRandom = async () => {
        this.setState({randomButtonDisabled: true});
        await this.handleBotCommand('shuffle');
        this.setState({randomButtonDisabled: false});
    }

    handleLoop = async () => {
        this.setState({loopButtonDisabled: true});
        await this.handleBotCommand('loop');
        this.setState({loopButtonDisabled: false});
    }

    componentWillUnmount() {
        this.stopTimeCalculating();
    }


    render() {
        return (this.props.app.playing &&
            <footer className="sticky-footer bg-black fixed-bottom">
                <div className="wm-player row">
                    <div className={"wm-player-infos-center col-sm-12 col-md-5 mb-3 d-md-none"}>
                        <div className={"wm-player-img"}>
                            {this.state.song.service === 'youtube' ?
                                <a href={this.state.song.url} target="_blank"><img src={this.state.song.image}
                                                                                   width="168" height="94"/></a>
                                :
                                this.state.song.service === 'soundcloud' ?
                                    <a href={this.state.song.url} target="_blank"><img src={this.state.song.image}
                                                                                       width="100" height="100"/></a>
                                    :
                                    <img src={this.state.song.image} width="100" height="100"/>
                            }
                        </div>
                        <div className={"wm-player-infos-titles-center"}>
                            <h2>{this.state.song.title}</h2>
                            <h3>{this.state.song.artist}</h3>
                        </div>
                    </div>
                    <div className={"wm-player-infos col-sm-12 col-md-5 mb-3 mb-md-0 d-none d-md-flex"}>
                        <div className={"wm-player-img"}>
                            {this.state.song.service === 'youtube' ?
                                <a href={this.state.song.url} target="_blank"><img src={this.state.song.image}
                                                                                   width="168" height="94"/></a>
                                :
                                this.state.song.service === 'soundcloud' ?
                                    <a href={this.state.song.url} target="_blank"><img src={this.state.song.image}
                                                                                       width="100" height="100"/></a>
                                    :
                                    <img src={this.state.song.image} width="100" height="100"/>
                            }
                        </div>
                        <div className={"wm-player-infos-titles"}>
                            <h2>{this.state.song.title}</h2>
                            <h3>{this.state.song.artist}</h3>
                        </div>
                    </div>
                    <div className={"wm-player-commands col-sm-12 col-md-7"}>
                        <div className={"wm-player-buttons"}>
                            <button onClick={this.handleLyricsClick} className="wm-not-button wm-control"><i
                                className={"fa fa-microphone-alt"}/></button>
                            <button onClick={this.handleRandom} disabled={this.state.randomButtonDisabled}
                                    className={`wm-not-button wm-control${this.state.random ? ' wm-control-active' : ''}`}><i className={"fa fa-random"}/></button>
                            <button onClick={this.handlePrevious} disabled={this.state.previousButtonDisabled}
                                    className="wm-not-button wm-control"><i className={"fa fa-step-backward"}/></button>
                            <button onClick={this.handlePlayPause} disabled={this.state.playPauseButtonDisabled}
                                    className="wm-not-button wm-control"><i className={"fa fa-" + (this.state.paused ? 'play' : 'pause')}/></button>
                            <button onClick={this.handleNext} disabled={this.state.nextButtonDisabled}
                                    className="wm-not-button wm-control"><i className={"fa fa-step-forward"}/></button>
                            <button onClick={this.handleLoop} disabled={this.state.loopButtonDisabled}
                                    className={`wm-not-button wm-control${this.state.loop ? ' wm-control-active' : ''}`}><i className={"fa fa-sync-alt"}/></button>
                        </div>
                        <input onMouseDown={this.handleChangingTime} onMouseUp={this.handleStopChangingTime}
                               onChange={this.handleTimeChange} disabled={this.state.timeChangingDisabled} type="range"
                               className="form-range" min="0"
                               max={this.state.song.length}
                               value={this.state.length} step="1"/>
                        <div className={"wm-player-timebar-times"}>
                            <div>{this.state.duration}</div>
                            <div>{this.state.song.duration}</div>
                        </div>
                    </div>
                </div>
            </footer>
        );
    }

}

export default withRouter(Player);