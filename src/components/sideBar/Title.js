const Title = (props) => <a className="sidebar-brand d-flex align-items-center justify-content-center" href="/">
    <div className="sidebar-brand-text mx-3">{props.children}</div>
</a>;
export default Title;