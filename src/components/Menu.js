import Link from "./menu/Link";

const Menu = () => <>
    <Link exact={true} to={"/"} title={"File d'attente"} icon={"music"} />
    <Link to={"/playlists"} title={"Playlists"} icon={"list-ul"} />
</>;
export default Menu;