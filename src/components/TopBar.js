import Search from "./topBar/Search";
import UserInfos from "./topBar/UserInfos";

const TopBar = (props) => <nav className="navbar navbar-expand navbar-dark bg-black topbar mb-4 static-top shadow">
    <button id="sidebarToggleTop" className="btn btn-link d-md-none rounded-circle mr-3">
        <i className="fa fa-bars" />
    </button>
    <Search app={props.app} />
    <div className="topbar-divider d-none d-sm-block" />
    <UserInfos app={props.app} />
</nav>;
export default TopBar;